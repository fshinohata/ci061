#!/bin/bash

FROM=''
TO=''

while [[ $# -gt 0 ]]
do
	key="$1"
	case $key in
		-h|--help)
			echo
			echo "Output with timestamps to HTML table converter script v1.0.0.0"
			echo "Options:"
			echo "    -f|--from <time>"
			echo "        Starting time. <time> must be in the HH:MM:SS format."
			echo "    -t|--to <time>"
			echo "        Final time. <time> must be in the HH:MM:SS format."
			echo
			exit
			;;
		-f|--from)
			FROM="$2"
			shift
			shift
			;;
		-t|--to)
			TO="$2"
			shift
			shift
			;;
		*)
			shift
			;;
	esac
done

if [ "$FROM" == "" -o "$TO" == "" ]; then
	echo "You must set the time range with the options '-f' and '-t'!"
	echo "Run with option '--help' if you need details."
fi

OUTPUT_FOLDER='../output'

TO=$(date -d "+1 second $TO" +%T)
FILES=$(ls -1 $OUTPUT_FOLDER/*.out)

echo "<table>"

while [ "$FROM" != "$TO" ]; do
	echo -e "\t<tr>"
		# Get all data related to "$FROM" and print into a row
		# echo -e "\t\t<td><pre><code>$FROM</code></pre></td>"
		for i in ${FILES[@]}; do
			data_entries=$(cat $OUTPUT_FOLDER/$i | grep "$FROM" | sed -e 's/ /--/g')
			echo -e "\t\t<td>"
				echo -e "\t\t\t<pre><code>"
					for data in ${data_entries[@]}; do
						echo -e "$(echo $data | sed -e 's/--/ /g')"
					done
				echo -e "\t\t\t</code></pre>"
			echo -e "\t\t</td>"
		done
		FROM=$(date -d "+1 second $FROM" +%T)
	echo -e "\t</tr>"
done

echo "</table>"
