# -*- coding: utf-8 -*-

"""Script that starts a single calculator client for testing.

You **NEED** to give a number to it as argument, which will be it's ID.

The script allows the creation of multiple clients with the same ID, but 
it's expected that the developer WON'T do that.

The purpose of this script is to test the server's response time and
the precision of it's answers.

It has some simple expressions with the expected answers for comparison.

In short, the client will wait a random [1..5] seconds interval, and
them will get a random expression and ask the server for it's evaluation result.

Every single step will be logged in the output file.
"""

if __name__ == '__main__':
    from classes import CalculatorClient
    import sys
    import time
    import random

    if len(sys.argv) != 2:
        print("Usage: python client.py <client_id>")
        exit()
    else:
        try:
            client_id = int(sys.argv[1])
        except ValueError:
            print("'{}' is NOT a number!".format(sys.argv[1]))

    # Opens output file
    output = open("../output/client_{}.out".format(client_id), "a")

    # Create a new client instance
    c = CalculatorClient(client_id, output)

    # Avaiable expressions and answers
    expressions = [
        { 'expression' : '3+5', 'answer' : '8'},
        { 'expression' : '3*5', 'answer' : '15'},
        { 'expression' : '3-5', 'answer' : '-2'},
        { 'expression' : '3/5', 'answer' : '0.6'},
        { 'expression' : '3**5', 'answer' : '243'},
        { 'expression' : '1+7', 'answer' : '8'},
        { 'expression' : '1*7', 'answer' : '7'},
        { 'expression' : '1-7', 'answer' : '-6'},
        { 'expression' : '1/7', 'answer' : '0.14285714285714285'},
        { 'expression' : '1**7', 'answer' : '1'}
    ]

    while True:
        time.sleep(random.randint(1, 5))

        # Get a random expression
        e = expressions[ random.randint(0, len(expressions)-1) ]
        
        # It's not the built-in eval()!
        # Asks for the result and compares 
        # the received answer with the 'real' answer
        c.eval(e['expression'], e['answer']) 




        