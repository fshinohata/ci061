#!/bin/bash

############################
# VARIABLES
############################

SIMULATION_TYPE="local"

HOSTS=("aphrodite.local" "ares.local" "hera.local" "eros.local")

RANDMAX=1024

# Where the log output files are
OUTPUT_FOLDER=../output

# Number of servers
NUM_SERVERS=3

# Number of clients
NUM_CLIENTS=1

# How much time (in seconds) at max the test should take
# It can increase depending on the value of TIMESPEED
RUNTIME=180

# How fast the simulation should pertake.
# 1 means 'normal', 0.5 means 'half', and so on.
TIMESPEED=1

# How many kills/resurrections the program should attempt
KILL_OR_RESURRECTION_ATTEMPTS=10

# Chance to kill/resurrect the selected target
KILL_OR_RESURRECT_CHANCE=1

TERMINAL="xterm"

# SERVER == array
declare -A SERVER

# CLIENT == array
declare -A CLIENT

############################
# FUNCTIONS
############################

function getrand() {
    echo $(( $RANDOM % $RANDMAX ))
}

function float() {
    echo $(echo "scale=15 ; $1" | bc)
}

function pid() {
    echo $(echo "$1" | sed -e 's/ [^ ]*$//g' | sed -e 's/ [^ ]*$//g')
}

function state() {
    echo $(echo "$1" | sed -e 's/^[^ ]* //g' | sed -e 's/ [^ ]*$//g')
}

function host() {
    echo $(echo "$1" | sed -e 's/^[^ ]* //g' | sed -e 's/^[^ ]* //g')
}

# function kill() {
#     if [ "$SIMULATION_TYPE" == "local" ]; then
        
#     elif [ "$SIMULATION_TYPE" == "network" ]; then
        
#     fi
# }

# function resurrect() {

# }

function log() {
    if [ "$SIMULATION_TYPE" == "local" ]; then
        echo "$1"
        echo "[$(date +%T)] [BASH] $1" >> $OUTPUT_FOLDER/server_$target.out
    elif [ "$SIMULATION_TYPE" == "network" ]; then
        ssh $2 "cd $PWD && echo \"[$(date +%T)] [BASH] $1\" >> $OUTPUT_FOLDER/server_$target.out"
    fi
}

function boot_server() {
    if [ "$SIMULATION_TYPE" == "local" ]; then
        ($TERMINAL -T "Server $1" -hold -e python3 $PWD/server.py $1) &
    elif [ "$SIMULATION_TYPE" == "network" ]; then
        (ssh $2 "cd $PWD && python3 $PWD/server.py $1") &
    fi
}

function boot_client() {
    if [ "$SIMULATION_TYPE" == "local" ]; then
        ($TERMINAL -T "Client $1" -hold -e python3 $PWD/client.py $1) &
    elif [ "$SIMULATION_TYPE" == "network" ]; then
        (ssh $2 "cd $PWD && python3 $PWD/client.py $1") &
    fi
}

function boot_all() {
    if [ "$SIMULATION_TYPE" == "local" ]; then
        # Start servers
        for((i = 1; i <= $NUM_SERVERS; i++));
        do
            echo "Starting server $i..."
            boot_server $i
            SERVER[$i]="$! alive none"
        done


        # Start clients
        for (( i = 1; i <= $NUM_CLIENTS; i++ )); do
            echo "Starting client $i..."
            boot_client $i
            CLIENT[$i]="$! alive none"
        done
    elif [ "$SIMULATION_TYPE" == "network" ]; then
        
        let k=0

        # Start servers
        for((i = 1; i <= $NUM_SERVERS; i++));
        do
            echo "Starting server $i..."
            boot_server $i ${HOSTS[$k]}
            SERVER[$i]="$! alive ${HOSTS[$k]}"
            k=$(( ($k + 1) % ${#HOSTS[@]} ))
        done


        # Start clients
        for (( i = 1; i <= $NUM_CLIENTS; i++ )); do
            echo "Starting client $i..."
            boot_client $i ${HOSTS[$k]}
            CLIENT[$i]="$! alive ${HOSTS[$k]}"
            k=$(( ($k + 1) % ${#HOSTS[@]} ))
        done
    fi
}

boot_all

let KILL_OR_RESURRECTION_ATTEMPTS_DONE=0

for (( i = 1; i <= $(( $RUNTIME / $TIMESPEED )); i++ )); do
    sleep $(float "1 / $TIMESPEED")

    if [ $(( $i % ($RUNTIME / $KILL_OR_RESURRECTION_ATTEMPTS) )) -eq 0 ]; then

        let KILL_OR_RESURRECTION_ATTEMPTS_DONE++

        if [ $KILL_OR_RESURRECTION_ATTEMPTS -gt 0 -a $KILL_OR_RESURRECTION_ATTEMPTS_DONE -le $KILL_OR_RESURRECTION_ATTEMPTS ]; then
            target=$(( $(getrand) % $NUM_SERVERS + 1 ))
            target_pid=$(pid "${SERVER[$target]}")
            target_state=$(state "${SERVER[$target]}")
            target_host=$(host "${SERVER[$target]}")
            target_will_die_or_resurrect=$(float "($(getrand) / $RANDMAX) <= $KILL_OR_RESURRECT_CHANCE")

            if [ "$target_will_die_or_resurrect" == "1" ]; then
                if [ "$target_state" == 'alive' ]; then
                    log "Killing server $target..." $target_host
                    kill $target_pid
                    SERVER[$target]="0 dead $target_host"
                else
                    log "Resurrecting server $target..." $target_host
                    boot_server $target $target_host
                    SERVER[$target]="$! alive $target_host"
                fi
            else
                if [ "$target_state" == 'alive' ]; then
                    echo "Attempt to kill server $target failed"
                else
                    echo "Attempt to resurrect server $target failed"
                fi
            fi
        fi
    fi
done

echo "Killing all remaining servers..."

for((i = 1; i <= $NUM_SERVERS; i++));
do
    pid=$(pid "${SERVER[$i]}")
    state=$(state "${SERVER[$i]}")
    host=$(host "${SERVER[$i]}")
    if [ "$state" == 'alive' ]; then
        log "Killing server $i..." $host
        # echo "[$(date +%T)] [BASH] Killing server $i..." >> $OUTPUT_FOLDER/server_$i.out
        kill $pid
        SERVER[$i]="0 dead $host"
    fi
done

echo "Killing all clients..."

for (( i = 1; i <= $NUM_CLIENTS; i++ )); do
    pid=$(pid "${CLIENT[$i]}")
    state=$(state "${CLIENT[$i]}")
    host=$(host "${CLIENT[$i]}")
    if [ "$state" == 'alive' ]; then
        log "Killing client $i..." $host
        # echo "[$(date +%T)] [BASH] Killing client $i..." >> $OUTPUT_FOLDER/client_$i.out
        kill $pid
        CLIENT[$i]="0 dead $host"
    fi
done
