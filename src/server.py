# -*- coding: utf-8 -*-

"""Script that starts a single calculator server.

You **NEED** to give a number to it as argument, which will be it's ID.

The script allows the creation of multiple servers with the same ID, but 
it's expected that the developer WON'T do that.

The method ``c.start()`` will put the server in a endless listening loop.

The server uses the built-in :py:func:`eval()` function to evaluate the
expressions and return the answers.

It only accepts expressions with numbers only. No variables are allowed.
"""

if __name__ == '__main__':
    from classes import CalculatorServer
    import sys
    import time

    # There must be exactly one argument!
    if len(sys.argv) != 2:
        print("Usage: python server.py <server_id>")
        exit()
    else:
        try:
            server_id = int(sys.argv[1])
        except ValueError:
            print("'{}' is NOT a number!".format(sys.argv[1]))

    # Opens output file
    output = open("../output/server_{}.out".format(server_id), "a")

    # Creates the server with the received ID and start it
    c = CalculatorServer(server_id, output)

    """
        Starts the server 
        This action blocks the server permanently
    """
    c.start()




